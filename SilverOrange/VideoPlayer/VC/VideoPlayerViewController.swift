//
//  VideoPlayerViewController.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit
import RxSwift

class VideoPlayerViewController: SharedViewController {
    
    let viewModel: VideoPlayerViewModel
    
    let disposeBag = DisposeBag()
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 0
        tbl.estimatedRowHeight = 200
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView()
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()

    
    init(viewModel: VideoPlayerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = GlobalSettings.shared().mainColor
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.compactAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        viewModel.viewDidLoad(self)
    }
    

    func layoutViews() {
        view.backgroundColor = .white
        addTableView()
    }
    
    func addTableView() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }


}
