//
//  VideoPlayerViewController+Ext.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit

extension VideoPlayerViewController: Viewable {
    
    func show(result: Result<Any, SOError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return
                }
                self?.configureTableView(sections)
                case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                print(error)
            }
            
        }

    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        subscribeOnTap()
    }
    
    func subscribeOnTap() {
        tableViewContainer.delegateHandler.passSelectedItem.subscribe { [weak self] (event) in
            guard let self = self, let model = event.element as? VideoModel else { return }
            self.viewModel.didSelectItem(item: model)
        }.disposed(by: disposeBag)
    }
}
