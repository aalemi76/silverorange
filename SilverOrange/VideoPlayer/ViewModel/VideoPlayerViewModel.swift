//
//  VideoPlayerViewModel.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
import Alamofire
class VideoPlayerViewModel: ViewModelProvider {
    
    typealias model = [VideoModel]
    
    let interactor: Interactor<[VideoModel]>
    
    weak var view: Viewable?
    
    var items = [VideoModel]()
    
    var sections = [Sectionable]()
    
    required init(interactor: Interactor<[VideoModel]>) {
        self.interactor = interactor
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        loadItems()
    }
    
    func loadItems() {
        let url = Routes.generate(.videos)
        interactor.getModel(url: url, parameters: nil) { [weak self] data in
            guard let self = self else { return }
            self.items = data
            self.prepareSections(items: data)
            self.view?.show(result: .success(self.sections))
        } onFailure: { [weak self] error in
            self?.view?.show(result: .failure(error))
        }
        
    }
    
    func prepareSections(items: [VideoModel]) {
        let playerCell = [PlayerCellViewModel(reuseID: PlayerTableViewCell.reuseID, cellClass: PlayerTableViewCell.self, model: items.first)]
        
        playerCell[0].delegate = self
        let otherItems = items.dropFirst()
        let cells = otherItems.map({ VideoTableCellViewModel(reuseID: VideoTableViewCell.reuseID, cellClass: VideoTableViewCell.self, model: $0)})
        sections = [SectionProvider(cells: playerCell, headerView: nil, footerView: nil),
                    SectionProvider(cells: cells, headerView: nil, footerView: nil)]
        }
    
    func didSelectItem(item: VideoModel) {
        guard let index = items.firstIndex(where: {$0 == item}) else { return }
        var newItems = items
        newItems.remove(at: index)
        newItems.insert(item, at: 0)
        prepareSections(items: newItems)
        view?.show(result: .success(self.sections))
    }

}

extension VideoPlayerViewModel: PlayerCellViewModelDelegate {
    
    func didTapNextBtn(item: VideoModel) {
        guard let index = items.firstIndex(where: {$0 == item}) else { return }
        var nextIndex = -1
        if index + 1 == items.count {
            nextIndex = 0
        } else {
            nextIndex = index + 1
        }
        didSelectItem(item: items[nextIndex])
    }
    
    func didTapPreviousBtn(item: VideoModel) {
        guard let index = items.firstIndex(where: {$0 == item}) else { return }
        var nextIndex = -1
        if index - 1 < 0 {
            nextIndex = items.count - 1
        } else {
            nextIndex = index - 1
        }
        didSelectItem(item: items[nextIndex])
    }
    
    
}
