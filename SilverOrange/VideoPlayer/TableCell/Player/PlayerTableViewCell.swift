//
//  PlayerTableViewCell.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit
import AVKit

class PlayerTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID: String = String(describing: PlayerTableViewCell.self)
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playerView: PlayerView!
    
    weak var viewModel: PlayerCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        playerView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? PlayerCellViewModel
        viewModel.cellDidLoad(self)
        return
    }
    
    func update(model: Any) {
        guard let model = model as? VideoModel else { return }
        titleLabel.text = model.title
        authorLabel.text = model.author.name
        descriptionLabel.text = model.videoDescription
        playerView.player.pause()
        playerView.configurePlayer(with: model.fullURL)
    }
    
}

extension PlayerTableViewCell: PlayerViewDelegate {
    
    func didTapNextBtn() {
        guard let item = viewModel?.getModel() as? VideoModel else { return }
        viewModel?.delegate?.didTapNextBtn(item: item)
    }
    
    func didTapPreviousBtn() {
        guard let item = viewModel?.getModel() as? VideoModel else { return }
        viewModel?.delegate?.didTapPreviousBtn(item: item)
    }
    
    
}
