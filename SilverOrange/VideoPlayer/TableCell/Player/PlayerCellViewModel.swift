//
//  PlayerCellViewModel.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation

protocol PlayerCellViewModelDelegate: AnyObject {
    func didTapNextBtn(item: VideoModel)
    func didTapPreviousBtn(item: VideoModel)
}

class PlayerCellViewModel: TableCellViewModel {
    
    weak var cell: Updatable?
    
    weak var delegate: PlayerCellViewModelDelegate?
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
    }
    
}
