//
//  PlayerView.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit
import AVFoundation

protocol PlayerViewDelegate: AnyObject {
    func didTapNextBtn()
    func didTapPreviousBtn()
}

class PlayerView: UIView {
    
    weak var delegate: PlayerViewDelegate?
    
    var player = AVPlayer()
    
    var playerLayer = AVPlayerLayer()
    
    var isPlaying = false
    
    var isFirstTime = true
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .whiteLarge)
        view.startAnimating()
        return view
    }()
    
    lazy var playButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "pause"), for: .normal)
        btn.tintColor = GlobalSettings.shared().mainColor
        btn.addTarget(self, action: #selector(playButtonTouchUpInside), for: .touchUpInside)
        btn.isHidden = true
        return btn
    }()
    
    lazy var nextButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "next"), for: .normal)
        btn.tintColor = GlobalSettings.shared().mainColor
        btn.addTarget(self, action: #selector(nextButtonTouchUpInside), for: .touchUpInside)
        btn.isHidden = true
        return btn
    }()
    
    lazy var previousButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "previous"), for: .normal)
        btn.tintColor = GlobalSettings.shared().mainColor
        btn.addTarget(self, action: #selector(previousButtonTouchUpInside), for: .touchUpInside)
        btn.isHidden = true
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func configurePlayer(with url: String) {
        guard let url = URL(string: url) else { return }
        player = AVPlayer(url: url)
        playerLayer.removeFromSuperlayer()
        playerLayer = AVPlayerLayer(player: player)
        layer.addSublayer(playerLayer)
        playerLayer.frame = CGRect(x: frame.minX, y: frame.minY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width*9/16)
        player.play()
        player.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
        layoutViews()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "currentItem.loadedTimeRanges" {
            activityIndicator.stopAnimating()
            isPlaying = true
            if isFirstTime {
                playButton.setImage(UIImage(named: "pause"), for: .normal)
                playButton.isHidden = false
                nextButton.isHidden = false
                previousButton.isHidden = false
            }
            isFirstTime = false
        }
    }
    
    func layoutViews() {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(tapGestureRecognizer:)))
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGestureRecognizer)
        isFirstTime = true
        
        
        backgroundColor = .black
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)])
        
        playButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(playButton)
        NSLayoutConstraint.activate([
            playButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            playButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            playButton.widthAnchor.constraint(equalToConstant: 50),
            playButton.heightAnchor.constraint(equalToConstant: 50 )])
        
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nextButton)
        NSLayoutConstraint.activate([
            nextButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 40),
            nextButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            nextButton.widthAnchor.constraint(equalToConstant: 50),
            nextButton.heightAnchor.constraint(equalToConstant: 50 )])
        
        previousButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(previousButton)
        NSLayoutConstraint.activate([
            previousButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -40),
            previousButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            previousButton.widthAnchor.constraint(equalToConstant: 50),
            previousButton.heightAnchor.constraint(equalToConstant: 50 )])
        
    }
    
    @objc func playButtonTouchUpInside() {
        
        if isPlaying {
            player.pause()
            playButton.setImage(UIImage(named: "play"), for: .normal)
        } else {
            player.play()
            playButton.setImage(UIImage(named: "pause"), for: .normal)
        }
        
        isPlaying = !isPlaying
    }
    
    @objc func nextButtonTouchUpInside() {
        delegate?.didTapNextBtn()
    }
    
    @objc func previousButtonTouchUpInside() {
        delegate?.didTapPreviousBtn()
    }
    
    @objc func didTapOnView(tapGestureRecognizer: UITapGestureRecognizer) {
        playButton.isHidden = !playButton.isHidden
        nextButton.isHidden = !nextButton.isHidden
        previousButton.isHidden = !previousButton.isHidden
    }
}
