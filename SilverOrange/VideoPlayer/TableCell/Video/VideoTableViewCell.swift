//
//  VideoTableViewCell.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-09.
//

import UIKit
import AVFoundation

class VideoTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID: String = String(describing: VideoTableViewCell.self)
    
    @IBOutlet weak var playerView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var videoDescriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        guard let model = model as? VideoModel else { return }
        titleLabel.text = model.title
        authorLabel.text = model.author.name
        videoDescriptionLabel.text = model.videoDescription
        return
    }
}
