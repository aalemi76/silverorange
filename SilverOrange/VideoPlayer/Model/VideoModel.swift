//
//  VideoModel.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation

// MARK: - VideoModel
struct VideoModel: Codable, Equatable {
    
    let id, title: String
    let hlsURL: String
    let fullURL: String
    let videoDescription, publishedAt: String
    let author: Author

    enum CodingKeys: String, CodingKey {
        case id, title, hlsURL, fullURL
        case videoDescription = "description"
        case publishedAt, author
    }
    
    static func == (lhs: VideoModel, rhs: VideoModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}

// MARK: - Author
struct Author: Codable {
    let id, name: String
}
