//
//  SOError.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
enum SOError: String, Error {
    case serverResponse = "Unable to communicate with server, please check your connection and try again."
    case parsingError = "Unable to parse received data to the specified model."
}
