//
//  ReusableView.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
