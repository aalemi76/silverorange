//
//  Updatable.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
protocol Updatable: AnyObject {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
