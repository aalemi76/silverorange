//
//  Sectionable.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit.UIView
protocol Sectionable: AnyObject {
    init(cells: [Reusable], headerView: UIView?, footerView: UIView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> UIView?
    func getFooterView() -> UIView?
    func append(_ cells: [Reusable])
    func removeCells()
}
