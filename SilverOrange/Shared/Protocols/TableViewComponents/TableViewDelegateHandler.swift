//
//  TableViewDelegateHandler.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit.UITableView
import RxSwift

class TableViewDelegateHandler: NSObject, UITableViewDelegate {
    private var sections: [Sectionable]
    var passSelectedItem = PublishSubject<Any>()
    var passSelectedSection = PublishSubject<Int>()
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = sections[section].getHeaderView()
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = sections[section].getFooterView()
        return footer
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.section != 0 else { return }
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.onNext(item)
    }
}
