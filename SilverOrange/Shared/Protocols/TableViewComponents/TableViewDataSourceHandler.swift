//
//  TableViewDataSourceHandler.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit.UITableView
import RxSwift

class TableViewDataSourceHandler: NSObject, UITableViewDataSource, UITableViewDelegate {
    var sections: [Sectionable]
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].getCells().count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].getCells()[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.getReuseID(), for: indexPath)
        (cell as? Updatable)?.attach(viewModel: item)
        (cell as? Updatable)?.update(model: item.getModel())
        return cell
    }
}
