//
//  ViewModelProvider.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation

protocol ViewModelProvider {
    associatedtype model: Codable
    init(interactor: Interactor<model>)
    func viewDidLoad(_ view: Viewable)
}
