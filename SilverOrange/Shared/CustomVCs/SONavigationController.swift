//
//  SONavigationController.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit

class SONavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: GlobalSettings.shared().systemFont(type: .bold, size: 18)]
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().mainColor
        navigationBar.tintColor = .white
        navigationBar.shadowImage = UIImage()
    }

}
