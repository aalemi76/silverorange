//
//  Routes.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation

enum Routes: String {
    case baseURL = "http://localhost:4000"
    case videos = "/videos"
    static func generate(_ route: Routes) -> String {
        return Routes.baseURL.rawValue + route.rawValue
    }

}
