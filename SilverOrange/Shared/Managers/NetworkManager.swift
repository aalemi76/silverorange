//
//  NetworkManager.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Alamofire
class NetworkManager {
    
    static let shared: NetworkManager = {
        return NetworkManager()
    }()
    
    typealias completionHandler = ((Result<Data, SOError>) -> Void)
    
    typealias RequestModifier = (inout URLRequest) throws -> Void
    
    var request: Alamofire.Request?
    var retryLimit: Int = 3
    
    func request(_ url: String, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.queryString, headers: HTTPHeaders? = nil, interceptor: RequestInterceptor? = nil, requestModifier: RequestModifier? = nil, completion: @escaping completionHandler) {
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers, interceptor: interceptor ?? self, requestModifier: requestModifier).validate().responseJSON { (response) in
            if let data = response.data {
                completion(.success(data))
            } else {
                completion(.failure(SOError.serverResponse))
            }
        }
    }
    
}
