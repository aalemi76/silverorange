//
//  Interactor.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
import Alamofire
class Interactor<Model: Codable> {
    typealias Object = Model
    func getModel(url: String, parameters: [String: Any]?,
                  onSuccess: @escaping (Object) -> Void,
                  onFailure: @escaping (SOError) -> Void) {
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers:
                    nil, interceptor: nil, requestModifier: nil).responseDecodable(of: Model.self, queue:
                    .global(qos: .background)) { response in
                        guard let object = response.value else {
                            onFailure(.parsingError)
                            return
                        }
                        onSuccess(object)
        }
    }
}
