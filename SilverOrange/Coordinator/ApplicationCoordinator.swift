//
//  ApplicationCoordinator.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//


import UIKit.UIViewController
class ApplicationCoordinator {
    let window: UIWindow
    init(window: UIWindow) {
        self.window = window
    }
    func start() -> Coordinator {
        let coordinator = VideoPlayerCoordinator(navigationController: SONavigationController())
        setRootViewcontroller(viewController: coordinator.navigationController, window: window)
        return coordinator
    }
    @available(iOS 13, *)
    func start(windowScene: UIWindowScene) -> Coordinator {
        let coordinator = VideoPlayerCoordinator(navigationController: UINavigationController())
        setRootViewcontroller(viewController: coordinator.navigationController, windowScene: windowScene)
        return coordinator
    }
    @available(iOS 13, *)
    private func setRootViewcontroller(viewController: UIViewController, windowScene: UIWindowScene) {
        window.windowScene = windowScene
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
    private func setRootViewcontroller(viewController: UIViewController, window: UIWindow) {
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
    
}
