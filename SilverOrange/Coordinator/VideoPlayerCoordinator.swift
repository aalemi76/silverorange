//
//  VideoPlayerCoordinator.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit.UINavigationController

class VideoPlayerCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    
    func start() {
        let vc = VideoPlayerViewController(viewModel: VideoPlayerViewModel(interactor: Interactor<[VideoModel]>()))
        vc.navigationItem.title = "Video Player"
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        push(viewController: vc, animated: true)
    }
    
    
}
