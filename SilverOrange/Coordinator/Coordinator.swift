//
//  Coordinator.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//


import UIKit.UIViewController

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    init(navigationController: UINavigationController)
    func start()
    func push(viewController: UIViewController, animated: Bool)
    func pop(animated: Bool)
    func present(viewController: UIViewController, animated: Bool)
    func dimiss(viewController: UIViewController, animated: Bool)
}

extension Coordinator {
    func push(viewController: UIViewController, animated: Bool) {
        navigationController.pushViewController(viewController, animated: animated)
    }
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    func present(viewController: UIViewController, animated: Bool) {
        navigationController.present(viewController, animated: true, completion: nil)
    }
    func dimiss(viewController: UIViewController, animated: Bool) {
        viewController.dismiss(animated: animated, completion: nil)
    }
}
